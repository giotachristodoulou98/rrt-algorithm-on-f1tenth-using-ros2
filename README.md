# RRT algorithm on F1TENTH using ROS2



### Name
F1/10: RRT algorithm simulation using ROS 2

### Description
This project explains the implementation of RRT algorithm on F1/10 autonomous system and the simulation of its movement in ROS Visualization 2 (RViz2).

# Installation
**Supported System**:
* Ubuntu 20.04 native with ROS 2

### Ubuntu 20.04 native
**ROS 2 setup (Foxy Fitzroy)**:

*Set locale*:

Make sure you have a locale which supports UTF-8. If you are in a minimal environment (such as a docker container), the locale may be something minimal like POSIX.
```bash
locale # check for UTF 8
sudo apt update && sudo apt install locales
sudo locale gen en_US en_US.UTF 8
sudo update locale LC_ALL=en_US.UTF 8 LANG=en_US.UTF 8
export LANG=en_US.UTF 8
locale # verify settings
```

*Add the ROS 2 apt repository*:

Ensure that the [Ubuntu Universe repository](https://help.ubuntu.com/community/Repositories/Ubuntu) is enabled.
```bash
sudo apt install software properties common
sudo add apt repository universe
```

*Add the ROS 2 GPG key with apt*:

```bash
sudo apt update && sudo apt install curl y
sudo curl sSL
https://raw.githubusercontent.com/ros/rosdistro/master
/ros.key o /usr/share/keyrings/ros archive
keyring.gpg
```

*Add the repository to your sources list*:

```bash
echo "deb [arch=$(dpkg print architecture) signed
by=/usr/share/keyrings/ros archive keyring.gpg]
http://packages.ros.org/ros2/ubuntu $(. /etc/os
release && echo $UBUNTU_CODENAME) main" | sudo tee
/etc/apt/sources.list.d/ros2.list > /dev/null
```

*Install development tools and ROS tools*:

```bash
sudo apt update && sudo apt install y
libbullet dev
python3 pip
python3 pytest cov
ros dev tools
# install some pip packages needed for testing
python3 m pip install U
argcomplete
flake8 blind except
flake8 builtins
flake8 class newline
flake8 comprehensions
flake8 deprecated
flake8 docstrings
flake8 import order
flake8 quotes
pytest repeat
pytest rerunfailures
pytest
# install Fast RTPS dependencies
sudo apt install no install recommends y
libasio dev
libtinyxml2 dev
# install Cyclone DDS dependencies
sudo apt install no install recommends y
libcunit1 dev
```

*Create a workspace and clone all repos*:

```bash
mkdir p ~/ros2_foxy/src
cd ~/ros2_foxy
vcs import input
https://raw.githubusercontent.com/ros2/ros2/foxy/ros2.repos
src
```

*Install dependencies using rosdep*:

```bash
sudo apt upgrade
sudo rosdep init
rosdep update
rosdep install from paths src ignore src y skip keys
"fastcdr rti connext dds 5.3.1 urdfdom_headers"
```

## Usage


## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
